<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02 - Loop Statements and Array Manipulation</title>
</head>
<body>
	<h1>Divisible by Five</h1>

	<p>
		<?php act2(); ?>
	</p>

	<h1>Array Manipulation</h1>

	<h2>Students in the Added Array</h2>

	<p>
		<?php 
			array_unshift($students, 'King'); 
		?>
	</p>

	<pre>
		<?php
			print_r($students);
		?>
	</pre>

</body>
</html>